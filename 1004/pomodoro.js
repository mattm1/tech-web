let timerCycles = -1;

// https://codepen.io/WalterNascimento/pen/yLOamWb
function Timer(startButton, stopButton, minutesSpan, secondsSpan, minutes, seconds, nextTimer){
  this.startButton = document.getElementById(startButton);
  this.stopButton = document.getElementById(stopButton);
  this.minutesSpan = document.getElementById(minutesSpan);
  this.secondsSpan = document.getElementById(secondsSpan);
  this.seconds = seconds;
  this.minutes = minutes;
  this.alreadyStarted = false;
  this.cron = null;
  this.nextTimer = nextTimer;
  this.battery = document.getElementById("battery");
  this.batteryWidth = 0;

  this.start = () => {
    if (timerCycles === 0) return;
    if (!this.alreadyStarted){
      this.alreadyStarted = true;
      this.cron = setInterval(() => {
        if (this.seconds === 0 && this.minutes === 0) {
          timerCycles-=1;

          // restore for next session
          this.seconds = seconds;
          this.minutes = minutes;
          this.alreadyStarted = false;
          this.battery.setAttribute("style","width:" + 0 + "px");
          this.batteryWidth = 0;
          clearInterval(this.cron);
          
          this.nextTimer.start(timerCycles);
          return;
        };
        if ((this.seconds + this.minutes*60 + 60) % 60=== 0){
          this.batteryWidth+=100;
          this.battery.setAttribute("style","width:" + this.batteryWidth + "px");
        }
        
        decreaseTime();
      }, 50); // TODO
    }
  };

  const decreaseTime = () => {
    if (this.seconds % 60 === 0){
      this.minutes-=1;
    }
    // https://stackoverflow.com/questions/4467539/javascript-modulo-gives-a-negative-result-for-negative-numbers
    this.seconds = (this.seconds -1 + 60) % 60;
    this.minutesSpan.innerText = this.minutes;
    this.secondsSpan.innerText = this.seconds;
  };

  this.startButton.addEventListener('click', this.start);

  this.stopButton.addEventListener('click', () => {
    // stop timer
    this.startButton.innerText = "resume";
    this.alreadyStarted = false;
    clearInterval(this.cron);
  });
  
}

document.getElementById('studyForm').addEventListener('submit', function(event) {
  // Previene il comportamento di default dell'evento, che sarebbe il submit del form
  event.preventDefault();
  // Ottiene il tempo di studio inserito dall'utente e lo converte in un numero intero
  const studySessions = parseInt(document.getElementById('studySessions').value, 10);
  const studyTime = parseInt(document.getElementById('studyTime').value, 10);
  const pauseTime = parseInt(document.getElementById('pauseTime').value, 10);
  
  let pauseTimer = new Timer("pauseStartButton", "pauseStopButton", "pauseMinutes", "pauseSeconds", pauseTime, 0, null, false);
  let studyTimer = new Timer("studyStartButton", "studyStopButton", "studyMinutes", "studySeconds", studyTime, 0, pauseTimer, true);
  pauseTimer.nextTimer = studyTimer;
  timerCycles = studySessions * 2;

  studyTimer.start();
});

